package com.codingraja.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@MultipartConfig(maxFileSize = 1024 * 1024 * 2, 
				 maxRequestSize = 1024 * 1024 * 10)
@WebServlet("/MultipleFileUploadServlet")
public class MultipleFileUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MultipleFileUploadServlet() {
		// Do Nothing
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("index.html").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		// Getting Files data
		Collection<Part> parts = request.getParts();

		// Getting Application Path
		String appPath = request.getServletContext().getRealPath("");

		// File path where all files will be stored
		String filePath = appPath + "docs";

		// Creates the file directory if it does not exists
		File fileDir = new File(filePath);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		String message = "All Files have been uploaded successfully!";
		
		//Checks Files are selected or not
		if(parts.isEmpty())
			message = "You did not select any file";

		// Writing Files Data
		for (Part part : parts) {
			try {
				part.write(filePath + File.separator + part.getSubmittedFileName());
			} catch (Exception ex) {
				message = "Exception: " + ex.getMessage();
			}
		}

		out.println("<h1 style=\"text-align:center;\">" + message + "</h1>");
	}
}
